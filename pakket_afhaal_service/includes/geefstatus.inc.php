<?php require 'php/connector.php';



$sql = "SELECT package_id, package_name, status_id FROM packages";
$stmt = $conn->prepare($sql);
$stmt->execute();
$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

// echo "<pre>";
// print_r($result);
// echo "</pre>";


?>
<div class="container mt-5">
  <table class="table table-striped table-bordered">
    <thead>
      <tr>
        <th scope="col">Pakketnaam</th>
        <th scope="col">Geef de status van het pakket aan</th>
      </tr>
    </thead>
    <tbody>
      <?php
      foreach ($result as $key => $value) {
      ?>
        <tr>
          <th><?php echo $value['package_name']; ?></th>
          <td>
            <div class="form-group">
              <form action="php/geefstatus.php" method="post">
                <select class="form-select" name="pakket_status" aria-label="Default select example">
                  <?php
                  $commandstring = "SELECT * FROM statuses ;";
                  $cmd = $conn->prepare($commandstring);
                  $cmd->execute();
                  $result = $cmd->fetchAll(PDO::FETCH_ASSOC);
                  foreach ($result as $row) {
                    if ($row['status_pakket']) {
                      $selected = '';
                      $disabled = '';
                      if ($row['status_id'] == $value['status_id']) {
                        $selected = 'selected="selected"';
                      }
                      echo '<option value= "' . $row['status_id'] . '" ' . $selected . '>' . $row['status_pakket'] . '</option>';
                    }
                  }
                  ?>
                </select>
          <td><button type="submit" class="btn btn-primary mb-3" name="btnstatus">Geef status</button></td>
          <input type="hidden" name="pakket_id" value="<?php echo $value['package_id']; ?>">
          </form>
        </tr>
      <?php } ?>
    </tbody>
  </table>
</div>