<?php
require 'php/connector.php';

$id = $_SESSION['gebruiker_id'];
$sql = "SELECT * FROM gebruikers 
LEFT JOIN rollen ON gebruikers.gebruiker_rol_id = rollen.rol_id
WHERE gebruiker_rol_id in (2,3,4)"; //selecteert alle gebruikers die de rol medewerker, scheidsrechter of beheerder hebben
$statement = $conn->prepare($sql);
$statement->execute([$id]);
$people = $statement->fetchAll(PDO::FETCH_ASSOC);
?>

<div class="container">
    <div class="card mt-5">
        <div class="card-header">
            <h2>Actoren</h2>
        </div>

        <div class="card-body">
            <table class="table table-bordered">
                <tr>
                    <th>Voornaam</th>
                    <th>Achternaam</th>
                    <th>Rol</th>
                    <th>Status</th>
                </tr>
                <?php foreach ($people as $person) : ?>
                    <tr>
                        <td><?= $person['gebruiker_voornaam']; ?></td>
                        <td><?= $person['gebruiker_achternaam']; ?></td>
                        <td><?= $person['rol_naam']; ?></td>
                        <?php if ($person['gebruiker_is_active'] == 0) { ?>
                            <td>Niet-actief</td>
                        <?php } elseif ($person['gebruiker_is_active'] == 1) { ?>
                            <td>Actief</td>
                        <?php } ?>
                        <td>

                            <a href="index.php?page=actoraanpassen&id=<?= $person['gebruiker_id']; ?>" class="btn btn-warning">Edit</a>

                            <a onclick="return confirm('Weet je zeker dat je dit team wilt verwijderen')" href="php/actorverwijderen.php?id=<?= $person['gebruiker_id'] ?>" class='btn btn-danger'>Verwijderen</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </div>
</div>