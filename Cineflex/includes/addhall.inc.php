</br>

<?php
if (!isset($_SESSION['loggedin'])) {
  $_SESSION['message'] = 'Make sure to login.';
  header('Location: index.php?page=login');
  exit();
}

$stmt = $conn->prepare("SELECT * FROM locations");
$stmt->execute();
$locaties = $stmt->fetchAll();
?>

<div class="container mt-5">
  <div class="col-md-4 offset-md-4">

    <h1>Add hall</h1>
    <hr>

    <form action="php/halladd.php" method="POST">
      <div class="form-floating mb-3" style="margin-top: 30px;">
        <input type="text" class="form-control" id="floatingInput" name="hall_number" placeholder="Hall number">
        <label for="floatingInput">Hallnumber</label>
      </div>
      <div class="form-floating mb-3" style="margin-top: 30px;">
        <input type="text" class="form-control" id="floatingInput" name="hall_row" placeholder="Number of rows">
        <label for="floatingInput">Number of rows</label>
      </div>
      <div class="form-floating mb-3" style="margin-top: 30px;">
        <input type="text" class="form-control" id="floatingInput" name="hall_chair" placeholder="Numbers of chairs">
        <label for="floatingInput">Number of chairs</label>
      </div>
      <div class="form-floating mb-3">
        <select class="form-select" aria-label="Select" name="locatie_id">
          <?php foreach ($locaties as $locatie) { ?>
            <option value="<?php echo $locatie['locatie_id']; ?>"><?php echo $locatie['locatie_naam'];
                                                                } ?></option>
        </select>
        <label for="floatingPassword">Location</label>
      </div>
      <button class="btn btn-primary" type="submit">Add</button>
    </form>
    </br>
    </form>

  </div>
</div>