<?php require 'dbh.php';
session_start();

$movie_name = $_POST['movie_name'];
$movie_genre = $_POST['movie_genre_id'];
$movie_duration = $_POST['movie_duration'];
$movie_description = $_POST['movie_description'];


if (empty($movie_name) || empty($movie_genre) || empty($movie_duration) || empty($movie_description)) {
    $_SESSION['message'] = "Fill in all fields";
    header("Location: ../index.php?page=addmovie");
    exit();
}

$sql = "INSERT INTO movies (movie_name, movie_genre_id, movie_duration, movie_description) VALUES (?,?,?,?)";
$conn->prepare($sql)->execute([$movie_name, $movie_genre, $movie_duration, $movie_description]);

$_SESSION['messageSuccess'] = "Movie has been added succesfully";

header("Location:../index.php?page=addmovie");
