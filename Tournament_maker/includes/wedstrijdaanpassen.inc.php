<?php
require 'php/connector.php';
$id = $_GET['id'];

$sql = "SELECT wedstrijd_id, t1.team_naam AS team1, t2.team_naam AS team2, tour.toernooi_naam, r.ronde_naam, s.gebruiker_voornaam, s.gebruiker_achternaam, score_team1, score_team2 FROM wedstrijden w
LEFT JOIN teams t1 ON w.team1_id = t1.team_id
LEFT JOIN teams t2 ON w.team2_id = t2.team_id
LEFT JOIN toernooien tour ON w.toernooi_id = tour.toernooi_id
LEFT JOIN rondes r ON w.ronde_id = r.ronde_id
LEFT JOIN gebruikers s ON w.scheidsrechter_id = s.gebruiker_id WHERE wedstrijd_id = $id";
$statement = $conn->prepare($sql);
$statement->execute([$id]);
$people = $statement->fetchAll(PDO::FETCH_ASSOC);


foreach ($conn->query($sql) as $value) {
    $value['wedstrijd_id'];
    $value['team1'];
    $value['team2'];
    $value['score_team1'];
    $value['score_team2'];
    $value['toernooi_naam'];
    $value['ronde_naam'];
?>

    <div class="container mt-5">
        <div class="col-md-4 offset-md-4">
            <form action="php/wedstrijdaanpassen.php" method="POST">
                <input type="hidden" value="<?php echo $value['wedstrijd_id']; ?>" name="wedstrijd_id">
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Team 1</label>
                    <input type="text" name="team1_id" class="form-control" value="<?php echo $value['team1']; ?>" readonly>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Team 2</label>
                    <input type="text" name="team2_id" class="form-control" value="<?php echo $value['team2']; ?>" readonly>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Score team 1</label>
                    <input type="int" name="score_team1" class="form-control" value="<?php echo $value['score_team1']; ?>" required>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlTextarea1"> Score Team 2</label>
                    <input type="int" name="score_team2" class="form-control" value="<?php echo $value['score_team2']; ?>" required>
                </div>
                <button type="submit" style="margin-top:10px; margin-left: 30%;" class="btn btn-primary">Pas wedstrijd aan</button>
            </form>
        </div>
    </div>
<?php } ?>