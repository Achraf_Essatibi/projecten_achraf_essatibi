<nav class="navbar navbar-expand-lg navbar-light bg-primary">
    <div class="container-fluid">

        <a class="navbar-brand" href="index.php?page=home">Cineflex <img src="images/PNGIX.com_production-icon-png_3830123.png" style="height: 40px;"></a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="index.php?page=home">Home</a>
                </li>

                <?php

                if (isset($_SESSION['loggedin'])) {

                    if ($_SESSION['user_role'] == 4) {
                ?>

                        <li class="nav-item">
                            <a class="nav-link" href="index.php?page=movieoverview">Movie overview</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="index.php?page=moviemanagement">Movie management</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="index.php?page=addmovie">Add movie</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="index.php?page=addlocation">Add location</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="index.php?page=addhall">Add hall</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="index.php?page=locationmanagement">Location / hall management</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="index.php?page=addperformance">Add performance</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="php/logout.php">Logout</a>
                        </li>

                    <?php
                    }
                }
                if (isset($_SESSION['loggedin'])) {

                    if ($_SESSION['user_role'] == 1) {
                    ?>
                        <li class="nav-item">
                            <a class="nav-link" href="php/logout.php">Logout</a>
                        </li>

                    <?php
                    }
                }
                if (isset($_SESSION['loggedin'])) {

                    if ($_SESSION['user_role'] == 2) {
                    ?>
                        <li class="nav-item">
                            <a class="nav-link" href="index.php?page=reserve">Reserve</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="index.php?page=seereservation">See reservation</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="php/logout.php">Logout</a>
                        </li>

                    <?php
                    }
                } else {
                    ?>
                    <li class="nav-item">
                        <a class="nav-link" href="index.php?page=register">Register</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="index.php?page=login">Login</a>
                    </li>
                <?php
                }

                ?>


            </ul>
        </div>
    </div>
</nav>