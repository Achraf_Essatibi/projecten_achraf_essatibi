<?php
require "connector.php";
session_start();
$toernooi_id = $_POST['toernooi_id'];
$team1_id = $_POST['team1_id'];
$team2_id = $_POST['team2_id'];
$ronde_id = $_POST['ronde_id'];
$wedstrijd_datum = $_POST['wedstrijd_datum'];
$scheidsrechter_id = $_POST['scheidsrechter_id'];

$sql = "SELECT * from wedstrijden WHERE team1_id = $team1_id AND team2_id = $team2_id AND ronde_id = $ronde_id";
$stmt = $conn->prepare($sql);
$stmt->execute();
$result = $stmt->fetch();

if ($result) {
    $_SESSION['message'] = "Deze teams spelen al tegen elkaar in deze ronde";
    echo $ronde_id;
    header("Location: ../index.php?page=wedstrijdentoevoegen");
    exit();
}

if ($team1_id == $team2_id) {
    $_SESSION['message'] = "Dezelfde teams kunnen niet tegen elkaar spelen";
    header("Location: ../index.php?page=wedstrijdentoevoegen");
    exit();
}

$sql = "INSERT INTO wedstrijden (toernooi_id, team1_id, team2_id, ronde_id, wedstrijd_datum, scheidsrechter_id) VALUES 
(:toernooi_id, :team1_id, :team2_id, :ronde_id, :wedstrijd_datum, :scheidsrechter_id)";
$data = [
    'toernooi_id' => $toernooi_id,
    'team1_id' => $team1_id,
    'team2_id' => $team2_id,
    'ronde_id' => $ronde_id,
    'wedstrijd_datum' => $wedstrijd_datum,
    'scheidsrechter_id' => $scheidsrechter_id
];
$conn->prepare($sql)->execute($data);
$_SESSION['messageSuccess'] = "De wedstrijd is toegevoegd";
header("Location:../index.php?page=bekijkwedstrijden");
