<?php
require 'php/dbh.php';
if ($_SESSION['user_role'] != 4) {
  $_SESSION['message'] = 'You dont have enough rights to visit this page.';
  header('Location: index.php?page=home');
  exit();
}

$sql = 'SELECT * FROM locations';
$statement = $conn->prepare($sql);
$statement->execute();
$people = $statement->fetchAll(PDO::FETCH_ASSOC);
?>

<div class="container">
  <div class="card mt-5">
    <div class="card-header">
      <h2>Location management</h2>
    </div>

    <div class="card-body">
      <table class="table table-bordered">
        <tr>
          <th>Location name</th>
          <th>Street</th>
          <th>Housenumber</th>
          <th>Postal code</th>
          <th>Place</th>
          <th>Actions</th>
        </tr>
        <?php foreach ($people as $person) : ?>
          <tr>
            <td><?= $person['location_name']; ?></td>
            <td><?= $person['location_street']; ?></td>
            <td><?= $person['location_housenumber']; ?></td>
            <td><?= $person['location_postalcode']; ?></td>
            <td><?= $person['location_place']; ?></td>
            <td>
              <a href="index.php?page=location_edit&id=<?= $person['location_id']; ?>" class="btn btn-warning">Edit</a>

              <a onclick="return confirm('Are you sure that you want to delete this location?')" href="php/deletelocation.php?id=<?= $person['location_id'] ?>" class='btn btn-danger'>Delete</a>
            </td>
          </tr>
        <?php endforeach; ?>
      </table>
    </div>
  </div>
</div>