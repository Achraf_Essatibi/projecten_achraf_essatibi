<div class="container mt-5">
    <div class="col-md-4 offset-md-4">
        <form action="php/teamtoevoegen.php" method="POST">
            <div class="form-group">
                <label for="exampleInputEmail1">Team naam</label>
                <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Vul de naam van het team in" name="team_naam" required>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Team afkorting</label>
                <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Vul de afkorting van het team in" name="team_afkorting" onkeypress="return /[a-z]/i.test(event.key)" required>
            </div>
            <div class="form-group">
                <label for="exampleFormControlFile1">Onderdeel afbeelding</label>
                <input type="file" class="form-control-file" id="exampleFormControlFile1" name="team_foto" required>
            </div>
            <button type="submit" style="margin-top:10px; margin-left: 33%;" class="btn btn-primary">Voeg team toe</button>
        </form>
    </div>
</div>