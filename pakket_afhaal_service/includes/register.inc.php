<form action="php/register.php" method="POST">
    <div class="container mt-5" >
        <div class="row">
            <div class="col-md-4 offset-md-4">
            <div class="form-group">
                    <label for="exampleInputEmail1">Gebruikersnaam</label>
                    <input type="text" name="username" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Vul je gebruikersnaam in" required>
                </div>
                <div class="form-group" required>
                    <label for="exampleInputEmail1">Email adres</label>
                    <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Vul je email in" required>
                </div>
                <div class="form-group" required>
                    <label for="exampleInputPassword1">Herhaal Wachtwoord</label>
                    <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Vul je wachtwoord in" required>
                </div>
                <div class="form-group" required>
                    <label for="exampleInputPassword1">Wachtwoord</label>
                    <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Vul je wachtwoord in" required>
                </div>
                <button type="submit" name="register" class="btn btn-primary">Registreer</button>
            </div>
        </div>
    </div>
</form>