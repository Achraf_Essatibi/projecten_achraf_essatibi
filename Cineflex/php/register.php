<?php
session_start();
require 'dbh.php';

$user_email = $_POST['user_email'];
$user_firstname = $_POST['user_firstname'];
$user_lastname = $_POST['user_lastname'];
$password = $_POST['user_password'];

$sql = "SELECT * from users WHERE user_email = ?";
$stmt = $conn->prepare($sql);
$stmt->execute([$user_email]);
$result = $stmt->fetch();


if (!filter_var($user_email, FILTER_VALIDATE_EMAIL)) {
    $_SESSION['message'] = "enter a valid email adress";
    header("Location: ../index.php?page=register");
    exit();
}


if ($result) {
    $_SESSION['message'] = "e-mail address is already in use";
    header("Location: ../index.php?page=register");
    exit();
}


if (empty($user_email) || empty($user_firstname) || empty($user_lastname) || empty($password)) {
    $_SESSION['message'] = "fill out all fields";
    header("Location: ../index.php?page=register");
    exit();
}

if (strlen($password) < 8) {
    $_SESSION['message'] = "Password must contain 8 characters";
    header("Location: ../index.php?page=register");
    exit();
}

if (!preg_match("#[0-9]+#", $password)) {
    $_SESSION['message'] = "Password must contain atleast 1 number";
    header("Location: ../index.php?page=register");
    exit();
}

if (!preg_match("#[A-Z]+#", $password)) {
    $_SESSION['message'] = "Password must contain atleast 1 capital letter";
    header("Location: ../index.php?page=register");
    exit();
}

$user_password = password_hash($_POST['user_password'], PASSWORD_DEFAULT);
$sql = "INSERT INTO users (user_email, user_password, user_firstname, user_lastname, user_role) VALUES (?,?,?,?, 2)";
$conn->prepare($sql)->execute([$user_email, $user_password, $user_firstname, $user_lastname]);

$_SESSION['messageSuccess'] = "Uw account is aangemaakt";

header("Location:../index.php?page=login");
