<?php require "connector.php";
session_start();
$id = $_POST['gebruiker_id'];
$gebruiker_is_active = $_POST['gebruiker_is_active'];


$sql = "UPDATE gebruikers SET gebruiker_is_active = :gebruiker_is_active WHERE gebruiker_id = $id";
$data = [
    'gebruiker_is_active' => $gebruiker_is_active
];
$conn->prepare($sql)->execute($data);
$_SESSION['messageSuccess'] = "De status van de gebruiker is aangepast";
header("Location:../index.php?page=actorenbekijken ");
