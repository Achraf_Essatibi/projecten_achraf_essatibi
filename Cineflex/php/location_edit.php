<?php

session_start();
include 'dbh.php';

$location_id = $_POST['location_id'];
$location_name = $_POST['location_name'];
$location_street = $_POST['location_street'];
$location_housenumber = $_POST['location_housenumber'];
$location_postalcode = $_POST['location_postalcode'];
$location_place = $_POST['location_place'];

if (empty($location_name) || empty($location_street) || empty($location_housenumber) || empty($location_postalcode || empty($location_place))) {
    $_SESSION['message'] = "Vul alle velden in";
    header("Location: ../index.php?page=location_edit&id=$location_id");
    exit();
}

$sql = "UPDATE locations SET location_name = ?, location_street = ?, location_housenumber = ?, location_postalcode = ?, location_place = ? WHERE location_id = ?";
$stmt = $conn->prepare($sql);
$stmt->execute([$location_name, $location_street, $location_housenumber, $location_postalcode, $location_place, $location_id]);
$_SESSION['messageSuccess'] = "Locatie is succesvol aangepast";
header("Location:../index.php?page=locationmanagement");
