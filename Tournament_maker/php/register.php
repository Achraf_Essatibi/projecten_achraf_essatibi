<?php
session_start();
require 'connector.php';

$gebruiker_voornaam = $_POST['gebruiker_voornaam'];
$gebruiker_achternaam = $_POST['gebruiker_achternaam'];
$gebruiker_gebruikersnaam = $_POST['gebruiker_gebruikersnaam'];
$gebruiker_email = $_POST['gebruiker_email'];
$gebruiker_wachtwoord = $_POST['gebruiker_wachtwoord'];

$sql = "SELECT * from gebruikers WHERE gebruiker_email = ?";
$stmt = $conn->prepare($sql);
$stmt->execute([$gebruiker_email]);
$result = $stmt->fetch();


if (!filter_var($gebruiker_email, FILTER_VALIDATE_EMAIL)) {
  $_SESSION['message'] = "enter a valid email adress";
  header("Location: ../index.php?page=registreren");
  exit();
}


if ($result) {
  $_SESSION['message'] = "e-mail address is already in use";
  header("Location: ../index.php?page=registreren");
  exit();
}


if (empty($gebruiker_email) || empty($gebruiker_voornaam) || empty($gebruiker_achternaam) || empty($gebruiker_email) || empty($gebruiker_wachtwoord)) {
  $_SESSION['message'] = "fill out all fields";
  header("Location: ../index.php?page=registreren");
  exit();
}

if (strlen($gebruiker_wachtwoord) < 15) {
  $_SESSION['message'] = "Password must contain 15 characters";
  header("Location: ../index.php?page=registreren");
  exit();
}

if (!preg_match("#[0-9]+#", $gebruiker_wachtwoord)) {
  $_SESSION['message'] = "Password must contain atleast 1 number";
  header("Location: ../index.php?page=registreren");
  exit();
}

if (!preg_match("#[A-Z]+#", $gebruiker_wachtwoord)) {
  $_SESSION['message'] = "Password must contain atleast 1 capital letter";
  header("Location: ../index.php?page=registreren");
  exit();
}
if (!preg_match("#[a-z]+#", $gebruiker_wachtwoord)) {
  $_SESSION['message'] = "Password must contain atleast 1 lowercase letter";
  header("Location: ../index.php?page=registreren");
  exit();
}
if (!preg_match("/[\'^£$%&*()}{@#~?><>,|=_+!-]/", $gebruiker_wachtwoord)) {
  $_SESSION['message'] = "Password must contain atleast 1 special character";
  header("Location: ../index.php?page=registreren");
  exit();
}

$sql = "INSERT INTO gebruikers (gebruiker_voornaam, gebruiker_achternaam, gebruiker_gebruikersnaam, gebruiker_email, 
gebruiker_wachtwoord, gebruiker_rol_id, gebruiker_is_active) VALUES 
(:voornaam, :achternaam, :gebruikersnaam, :email, :wachtwoord, :gebruiker_rol_id, :gebruiker_is_active)";
$data = [
  'voornaam' => $gebruiker_voornaam,
  'achternaam' => $gebruiker_achternaam,
  'gebruikersnaam' => $gebruiker_gebruikersnaam,
  'email' => $gebruiker_email,
  'wachtwoord' => password_hash($gebruiker_wachtwoord, PASSWORD_DEFAULT),
  'gebruiker_rol_id' => 1,
  'gebruiker_is_active' => 1

];
$conn->prepare($sql)->execute($data);
$_SESSION['messageSuccess'] = "Uw account is aangemaakt";
header("Location:../index.php?page=login");
