<?php require 'php/connector.php';



$sql = "SELECT * FROM packages INNER JOIN afmetingen ON afmetingen.afmetingen_id = packages.package_size";
$stmt = $conn->prepare($sql);
$stmt->execute();
$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

// echo "<pre>";
// print_r($result);
// echo "</pre>";


?>
<div class="container mt-5">
  <table class="table table-striped table-bordered">
    <thead>
      <tr>
        <th scope="col">Pakketnaam</th>
        <th scope="col">Heeft het pakket spoed?</th>
        <th scope="col">Afmetingen</th>
        <th scope="col">Prijs van het pakket</th>
      </tr>
    </thead>
    <tbody>
      <?php
      foreach ($result as $key => $value) {
        if ($value['package_important'] == 0) {
          $spoed = 'nee';
        } else {
          $spoed = 'ja';
        }

        $disabled = "";
        $buttonText = "Claim pakket";
        if ($value['claim_id']) {
          $disabled = 'style="pointer-events: none;"';
          $buttonText = "Al geclaimed";
        }
      ?>
        <tr>

          <input type="hidden" name="pakket_totaalprijs" id="priceTotal" value="<?php ?>" />

          <th><?php echo $value['package_name']; ?></th>
          <td><?php echo $spoed; ?></td>
          <td><?php echo $value['afmetingen']; ?></td>
          <td><?php echo $value['package_price']; ?></td>
          <td><a <?= $disabled ?> class="btn btn-primary" href="php/claimpakket.php?pakket=<?php echo $value['package_id']; ?>&user=<?php echo $_SESSION['user_id']; ?>"><?= $buttonText ?></a></td>
        </tr>
      <?php
      }
      ?>
    </tbody>
  </table>
</div>