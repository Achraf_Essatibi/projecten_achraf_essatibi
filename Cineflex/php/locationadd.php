<?php require 'dbh.php';
session_start();

$location_name = $_POST['location_name'];
$location_street = $_POST['location_street'];
$location_housenumber = $_POST['location_housenumber'];
$location_postalcode = $_POST['location_postalcode'];
$location_place = $_POST['location_place'];


if (empty($location_name) || empty($location_street) || empty($location_housenumber) || empty($location_postalcode || empty($location_place))) {
    $_SESSION['message'] = "Fill in all fields";
    header("Location: ../index.php?page=locatietoevoegen");
    exit();
}

$sql = "INSERT INTO locations (location_name, location_street, location_housenumber, location_postalcode, location_place) VALUES (?,?,?,?,?)";
$conn->prepare($sql)->execute([$location_name, $location_street, $location_housenumber, $location_postalcode, $location_place]);

$_SESSION['messageSuccess'] = "Location is added succesfully";

header("Location:../index.php?page=addlocation");
