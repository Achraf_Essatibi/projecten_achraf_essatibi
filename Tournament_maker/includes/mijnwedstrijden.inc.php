<?php
require 'php/connector.php';

$scheidsrechter_id = $_SESSION['gebruiker_id'];

$sql = "SELECT wedstrijd_id, t1.team_naam AS team1, t2.team_naam AS team2, tour.toernooi_naam, r.ronde_naam, s.gebruiker_voornaam, s.gebruiker_achternaam, score_team1, score_team2 FROM wedstrijden w
LEFT JOIN teams t1 ON w.team1_id = t1.team_id
LEFT JOIN teams t2 ON w.team2_id = t2.team_id
LEFT JOIN toernooien tour ON w.toernooi_id = tour.toernooi_id
LEFT JOIN rondes r ON w.ronde_id = r.ronde_id
LEFT JOIN gebruikers s ON w.scheidsrechter_id = s.gebruiker_id WHERE scheidsrechter_id = $scheidsrechter_id";
$statement = $conn->prepare($sql);
$statement->execute();
$people = $statement->fetchAll(PDO::FETCH_ASSOC);
?>

<div class="container">
    <div class="card mt-5">
        <div class="card-header">
            <h2>Wedstrijden</h2>
        </div>

        <div class="card-body">
            <table class="table table-bordered">
                <tr>
                    <th>Wedstrijd</th>
                    <th>Toernooi naam</th>
                    <th>Ronde</th>
                    <th>Scheidsrechter</th>
                </tr>
                <?php foreach ($people as $person) : ?>
                    <tr>
                        <td><?= $person['team1']; ?> VS <?= $person['team2']; ?> <?= $person['score_team1']; ?> - <?= $person['score_team2']; ?></td>
                        <td><?= $person['toernooi_naam']; ?></td>
                        <td><?= $person['ronde_naam']; ?></td>
                        <td><?= $person['gebruiker_voornaam'];
                            echo " ";
                            echo $person['gebruiker_achternaam']; ?></td>
                        <td>
                            <?php if (isset($_SESSION['loggedin'])) {
                                if ($_SESSION['gebruiker_rol_id'] == 3) { ?>
                                    <a href="index.php?page=wedstrijdaanpassen&id=<?= $person['wedstrijd_id']; ?>" class="btn btn-warning">Edit</a>

                                    <a onclick="return confirm('Weet je zeker dat je dit team wilt verwijderen')" href="php/wedstrijdverwijderen.php?id=<?= $person['wedstrijd_id'] ?>" class='btn btn-danger'>Verwijderen</a>
                        </td>
                <?php
                                }
                            } ?>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </div>
</div>