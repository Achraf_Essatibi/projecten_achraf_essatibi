</br>
<?php
if (!isset($_SESSION['loggedin'])) {
  $_SESSION['message'] = 'Make sure to login.';
  header('Location: index.php?page=login');
  exit();
}

$stmt = $conn->prepare("SELECT * FROM locations");
$stmt->execute();
$genres = $stmt->fetchAll();
?>

<div class="container mt-5">
  <div class="col-md-4 offset-md-4">

    <h1>Add location / hall </h1>
    <hr>

    <form action="php/locationadd.php" method="POST">
      <div class="form-floating mb-3" style="margin-top: 30px;">
        <input type="text" class="form-control" id="floatingInput" name="location_name" placeholder="location name">
        <label for="floatingInput">Location name</label>
      </div>
      <div class="form-floating mb-3">
        <input type="text" class="form-control" id="floatingInput" name="location_street" placeholder="Street">
        <label for="floatingInput">Street</label>
      </div>
      <div class="form-floating mb-3">
        <input type="text" class="form-control" id="floatingInput" name="location_housenumber" placeholder="House number">
        <label for="floatingInput">House number</label>
      </div>
      <div class="form-floating mb-3">
        <input type="text" class="form-control" id="floatingInput" name="location_postalcode" placeholder="Postal code">
        <label for="floatingInput">Postal code</label>
      </div>
      <div class="form-floating mb-3">
        <input type="text" class="form-control" id="floatingInput" name="location_place" placeholder="Place">
        <label for="floatingInput">Place</label>
      </div>
      <button class="btn btn-primary" type="submit">Add</button>
    </form>
    </br>
    </form>

  </div>
</div>