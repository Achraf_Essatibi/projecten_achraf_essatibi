<?php require 'php/connector.php';

$sql = "SELECT * FROM packages as p LEFT JOIN statuses as s ON p.status_id = s.status_id
        WHERE user_id = ?";
$stmt = $conn->prepare($sql);
$stmt->execute([$_SESSION['user_id']]);
$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

// echo "<pre>";
// print_r($result);
// echo "</pre>";


?>
<div class="container">
  <div class="card mt-5">
    <div class="card-header">
      <h2>Bekijk pakket</h2>
    </div>
    <div class="card-body">
      <table class="table table-bordered">
        <tr>
          <th>Pakketnaam</th>
          <th>Status</th>
          <th>Probleem</th>
        </tr>
        <?php
        foreach ($result as $key => $value) {
        ?>
          <tr>
            <th><?php echo $value['package_name']; ?></th>
            <td><?php echo $value['status_pakket']; ?></td>
            <?php if ($value['status_id'] == 7) { ?>
              <td><?php echo $value['packet_problem']; ?></td>
            <?php } else { ?>
              <td><?php echo 'Uw pakket bevat geen problemen' ?></td>
            <?php } ?>
          </tr>
        <?php } ?>
      </table>
    </div>
  </div>
</div>
</div>