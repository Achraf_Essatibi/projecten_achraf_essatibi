<div class="container mt-5">
    <div class="col-md-4 offset-md-4">
        <form action="php/register.php" method="POST">
            <h1>Registreren</h1>
            <p> De velden die een * bevatten zijn verplicht!</p>
            <div class="form-group">
                <label for="exampleInputVoornaam1">Voornaam*</label>
                <input type="text" class="form-control" id="exampleInputVoornaam1" placeholder="Voornaam" name="gebruiker_voornaam" onkeypress="return /[a-z]/i.test(event.key)" required>
            </div>
            <div class="form-group">
                <label for="exampleInputAchternaam1">Achternaam*</label>
                <input type="text" class="form-control" id="exampleInputAchternaam1" placeholder="Achternaam" name="gebruiker_achternaam" onkeypress="return /[a-z]/i.test(event.key)" required>
            </div>
            <div class="form-group">
                <label for="exampleInputAchternaam1">Gebruikersnaam*</label>
                <input type="text" class="form-control" id="exampleInputAchternaam1" placeholder="Achternaam" name="gebruiker_gebruikersnaam" required>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Email adres*</label>
                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Vul je emailadres in" name="gebruiker_email" required>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Wachtwoord*</label>
                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="gebruiker_wachtwoord" minlength="15" required>
            </div>
            <button type="submit" class="btn btn-primary" style="margin-top: 10px; margin-left:40%">Registreer</button>
        </form>
    </div>
</div>