<?php
include 'connector.php';
session_start();
if (isset($_POST['login_button'])) {
    $email = $_POST['login_email'];
    $password = $_POST['login_password'];

    if (empty($email) || empty($password)) {
        $_SESSION['melding'] = "Niet alle velden ingevuld";
        header("location: ../index.php?page=login");
    }

    // check if password is longer than 8 characters otherwise give error
    if (strlen($password) < 8) {
        $_SESSION['melding'] = "Wachtwoord moet minimaal 8 karakters lang zijn";
        header("location: ../index.php?page=login");
    }


    $sql = "SELECT * FROM users WHERE email = ?";
    $statement = $conn->prepare($sql);
    $statement->execute([$email]);
    $res = $statement->fetch(PDO::FETCH_ASSOC);

    if (password_verify($password, $res['password'])) {
        $_SESSION['loggedIn'] = true;
        $_SESSION['user_id'] = $res['user_id'];
        $_SESSION['role'] = $res['role_id'];
        $_SESSION['messageSuccess'] = "succesvol ingelogd";
        header("location: ../index.php?page=home");
    } else {
        $_SESSION['message'] = "Verkeerd wachtwoord";
        header("location: ../index.php?page=login");
    }
}
