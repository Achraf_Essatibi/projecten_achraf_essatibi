<?php require 'dbh.php';
session_start();

$user_email = $_POST['user_email'];
$user_password = $_POST['user_password'];

$sql = "SELECT * from users WHERE user_email = ?";
$stmt = $conn->prepare($sql);
$stmt->execute([$user_email]);

$result = $stmt->fetch();

if (empty($user_email) || empty($user_password)) {
    $_SESSION['message'] = "Vul alle velden in";
    header("Location: ../index.php?page=login");
    exit();
}   


if (password_verify($user_password, $result['user_password'])) {
    $_SESSION['loggedin'] = true;
    $_SESSION['user_id'] = $result['user_id'];
    $_SESSION['user_role'] = $result['user_role'];
    $_SESSION['messageSuccess'] = "Youre logged in";
    header("Location:../index.php?page=home");
} else {
    $_SESSION['message'] = "Your username or email is false";
    header("Location:../index.php?page=login");
}
