<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand"><img src="images/pakket-afhaal-service.png" height="100px"></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="index.php?page=home">Home</a>
        </li>

        <?php if (!isset($_SESSION['loggedIn'])) { ?>
          <li class="nav-item">
            <a class="nav-link" href="index.php?page=login">Login</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="index.php?page=register">Register</a>
          </li>
        <?php } else { ?>
          <?php if ($_SESSION['role'] == 1) { ?>
            <li class="nav-item">
              <a class="nav-link" href="index.php?page=claimpakket">Pakket claimen</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="index.php?page=geefstatus">Status geven</a>
            </li>
          <?php } ?>
          <?php if ($_SESSION['role'] == 2) { ?>
            <li class="nav-item">
              <a class="nav-link" href="index.php?page=claimpakket">Pakket claimen</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="index.php?page=geefstatus">Status geven </a>
            </li>
          <?php } ?>
          <?php if ($_SESSION['role'] == 3) { ?>
            <li class="nav-item">
              <a class="nav-link" href="index.php?page=pakketaanmelden">Pakket aanmelden</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="index.php?page=bekijkpakket">Pakket bekijken</a>
            </li>
          <?php } ?>


          <li class="nav-item">
            <a class="nav-link" href="php/logout.php">Logout</a>
          </li>
        <?php } ?>
      </ul>
    </div>
  </div>
</nav>