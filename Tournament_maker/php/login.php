<?php require 'connector.php';
session_start();

$gebruiker_email = $_POST['gebruiker_email'];
$gebruiker_wachtwoord = $_POST['gebruiker_wachtwoord'];

$sql = "SELECT * from gebruikers WHERE gebruiker_email = ?";
$stmt = $conn->prepare($sql);
$stmt->execute([$gebruiker_email]);

$result = $stmt->fetch();

if (empty($gebruiker_email) || empty($gebruiker_wachtwoord)) {
    $_SESSION['message'] = "Vul alle velden in";
    header("Location: ../index.php?page=login");
    exit();
}


if (password_verify($gebruiker_wachtwoord, $result['gebruiker_wachtwoord'])) {
    $_SESSION['loggedin'] = true;
    $_SESSION['gebruiker_id'] = $result['gebruiker_id'];
    $_SESSION['gebruiker_rol_id'] = $result['gebruiker_rol_id'];
    $_SESSION['messageSuccess'] = "Youre logged in";
    header("Location:../index.php?page=home");
} else {
    $_SESSION['message'] = "Your username or email is false";
    header("Location:../index.php?page=login");
}
