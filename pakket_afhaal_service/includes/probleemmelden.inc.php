<?php 
$id = $_GET['id'];
?>

<form method="post" action="php/probleemmelden.php">
    <input type="hidden" name="pakket_id" value="<?php echo $id;?>">

    <div class="container mt-5" >   
        <div class="row">
            <div class="col-md-4 offset-md-4">
                <div class="mb-3" style="text-align: center;">
                    <label for="exampleFormControlTextarea1" class="form-label"><b>Meld het probleem van het pakket</b></label>
                    <textarea class="form-control" name="pakket_probleem" id="exampleFormControlTextarea1" rows="3"></textarea>
                    <button type="submit" name="probleem_button" class="btn btn-danger mt-3">Meld het probleem</button>
                </div>
            </div>
        </div>
    </div>
</form>