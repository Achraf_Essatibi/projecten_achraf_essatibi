<?php require "php/connector.php";
$id = $_GET['id'];
$sql = "SELECT * FROM teams WHERE team_id = $id";
$stmt = $conn->prepare($sql);
$stmt->execute([$id]);

foreach ($conn->query($sql) as $value) {
    $value['team_id'];
    $value['team_naam'];
    $value['team_afkorting'];
    $value['team_foto'];
?>

    <div class="container mt-5">
        <div class="col-md-4 offset-md-4">
            <form action="php/teamaanpassen.php" method="POST">
                <input type="hidden" value="<?php echo $value['team_id']; ?>" name="team_id">
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Team naam</label>
                    <input type="text" name="team_naam" class="form-control" value="<?php echo $value['team_naam']; ?>" onkeypress="return /[a-z]/i.test(event.key)">
                </div>
                <div class="form-group">
                    <label for="exampleInputMerk1">Team afkorting</label>
                    <input type="text" name="team_afkorting" class="form-control" value="<?php echo $value['team_afkorting']; ?>" onkeypress="return /[a-z]/i.test(event.key)">
                </div>
                <div class="form-group">
                    <label for="exampleFormControlFile1">Team foto</label>
                    <input type="file" class="form-control-file" id="exampleFormControlFile1" name="team_foto" value="images/<?php echo $value['team_foto']; ?>">
                </div>
                <button type="submit" style="margin-top:10px; margin-left: 30%;" class="btn btn-primary">Pas team aan</button>
            </form>
        </div>
    </div>
<?php } ?>