<?php require "connector.php";
session_start();
$wedstrijd_id = $_POST['wedstrijd_id'];
$score_team1 = $_POST['score_team1'];
$score_team2 = $_POST['score_team2'];

$sql = "UPDATE wedstrijden SET score_team1 = :score_team1,
score_team2 = :score_team2 WHERE wedstrijd_id = $wedstrijd_id";
$data = [
    'score_team1' => $score_team1,
    'score_team2' => $score_team2,

];
$conn->prepare($sql)->execute($data);
$_SESSION['messageSuccess'] = "De wedstrijd is aangepast";
header("Location:../index.php?page=bekijkwedstrijden ");
