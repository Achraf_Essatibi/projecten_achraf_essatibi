<?php require "php/connector.php";
$id = $_GET['id'];
$sql = "SELECT * FROM gebruikers WHERE gebruiker_id = $id"; //Seleteert gegevens van de gebruiker die geselcteerd is door de beheerder
$stmt = $conn->prepare($sql);
$stmt->execute([$id]);

foreach ($conn->query($sql) as $value) { //Loopt door de array van gebruikers om in de form waarden op het scherm te laten zien
    $value['gebruiker_voornaam'];
    $value['gebruiker_achternaam'];
    $value['gebruiker_is_active'];
?>

    <div class="container mt-5">
        <div class="col-md-4 offset-md-4">
            <form action="php/actoraanpassen.php" method="POST">
                <input type="hidden" value="<?php echo $value['gebruiker_id']; ?>" name="gebruiker_id">
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Voornaam</label>
                    <input type="text" name="gebruiker_voornaam" class="form-control" value="<?php echo $value['gebruiker_voornaam']; ?>" readonly>
                </div>
                <div class="form-group">
                    <label for="exampleInputMerk1">Achternaam</label>
                    <input type="text" name="gebruiker_achternaam" class="form-control" value="<?php echo $value['gebruiker_achternaam']; ?>" readonly>
                </div>
                <div class="form-group">
                    <label for="exampleInputMerk1">Zet actor op actief of non-actief</label>
                    <select class="form-select" name="gebruiker_is_active">
                        <option value="0">Niet-actief</option>
                        <?php if ($value['gebruiker_is_active'] == 0) { ?>
                            <option value="1">Actief</option>
                        <?php } ?>
                    </select>
                </div>
                <button type="submit" style="margin-top:10px; margin-left: 30%;" class="btn btn-primary">Wijzig status</button>
            </form>
        </div>
    </div>
<?php } ?>