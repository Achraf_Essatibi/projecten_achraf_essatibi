<?php

session_start();
include 'dbh.php';

$movie_id = $_POST['movie_id'];
$movie_name = $_POST['movie_name'];
$movie_genre_id = $_POST['movie_genre_id'];
$movie_duration = $_POST['movie_duration'];
$movie_description = $_POST['movie_description'];

if (empty($movie_name) || empty($movie_genre_id) || empty($movie_duration) || empty($movie_description)) {
    $_SESSION['message'] = "Fill in all fields";
    header("Location: ../index.php?page=edit&id=$movie_id");
    exit();
}

$sql = "UPDATE movies SET movie_name = ?, movie_genre_id = ?, movie_duration = ?, movie_description = ? WHERE movie_id = ?";
$stmt = $conn->prepare($sql);
$stmt->execute([$movie_name, $movie_genre_id, $movie_duration, $movie_description, $movie_id]);
$_SESSION['messageSuccess'] = "Movie has been edited succesfully";
header("Location:../index.php?page=moviemanagement");
