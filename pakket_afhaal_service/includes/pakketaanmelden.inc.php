<form action="php/meldpakketaan.php" method="post">
  <input type="hidden" name="user_id" value="<?php echo $_SESSION['user_id']; ?> " />
  <div class="container mt-3">
    <h1 style="text-align: center;">Meld pakket aan</h1>
    <div class="form-floating">
      <div class="input-group">
        <span class="input-group-text">Geef hier de naam van het pakket </span>
        <textarea class="form-control" aria-label="With textarea" name="pakket_naam" required></textarea>
      </div>
    </div>
    <div class="form-floating mt-2">
      <select onchange="getPrice()" name="pakket_afmetingen" class="form-select" id="afmeting" aria-label="Floating label select example">
        <option value="1">0-10kg - 38 x 26.5 x 3.2 cm</option>
        <option value="2">10-30kg - 176 x 78 x 58 cm</option>
      </select>
      <label for="floatingSelect">Afmetingen</label>
    </div>
    <div class="form-floating mt-2">
      <select onchange="getPrice()" name="pakket_verzekerd" class="form-select" id="insured" aria-label="Floating label select example">
        <option selected value="1">Ja</option>
        <option value="2">Nee</option>
      </select>
      <label for="floatingSelect">Wilt u het pakket verzekerd hebben?</label>
    </div>
    <div onchange="getPrice()" class="form-floating mt-2">
      <select class="form-select" name="pakket_spoed" id="spoed" aria-label="Floating label select example">
        <option selected value="1">Ja</option>
        <option value="0">Nee</option>
      </select>
      <label for="floatingSelect">Wilt u het pakket met spoed aanmelden?</label>
    </div>
    <div class="form-floating mt-2">
      <input onchange="getPrice()" name="pakket_aantal" type="number" id="amount" class="form-control"></input>
      <label for="floatingSelect">Hoeveel pakketen wilt u aanmelden?</label>
    </div>
    <div class="form-floating mt-2">
      <h2>Uw prijs is : <span id="totalPrice"></span></h2>
      <input type="hidden" name="pakket_totaalprijs" id="priceTotal" value="" />
    </div>
    <button disabled class="btn btn-primary" name="pakket_aanmelden" id="submit" type="submit">Meld pakket aan</button>
  </div>
</form>




<script>
  function getPrice(el) {
    size = document.getElementById('afmeting').value;
    if (size == 1) {
      var price = 5.85;
    } else {
      var price = 11.44
    }

    if (isInsured() && isSpoed()) {
      price = price * 1.2;
    }

    if (isSpoed()) {
      price = price * 1.1
    }

    if (isInsured()) {
      price = price * 1.1
    }

    var quantity = howMany();
    price = price * quantity;

    document.getElementById("totalPrice").innerHTML = Math.round(price * 100) / 100;
    document.getElementById("priceTotal").value = price;

    if (price > 0) {
      document.getElementById("submit").disabled = false;
    } else {
      document.getElementById("submit").disabled = true;
    }
  }

  function isInsured() {
    var insured = document.getElementById("insured").value;
    if (insured == 1) {
      return true;
    }
    return false;
  }

  function isSpoed() {
    var spoed = document.getElementById("spoed").value;
    if (spoed == 1) {
      return true;
    }
    return false;
  }

  function howMany() {
    var amount = document.getElementById("amount").value;
    if (amount) {
      return amount;
    }
    return 0;
  }
</script>