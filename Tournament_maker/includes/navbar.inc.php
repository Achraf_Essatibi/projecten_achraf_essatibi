<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">

    <a class="navbar-brand">Tournament maker</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="index.php?page=home">Home</a>
        </li>

        <?php

        if (isset($_SESSION['loggedin'])) {

          if ($_SESSION['gebruiker_rol_id'] == 1) {
        ?>

            <li class="nav-item">
              <a class="nav-link" href="index.php?page=bekijkwedstrijden">Wedstrijden bekijken</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="php/logout.php">Logout</a>
            </li>

          <?php
          }
        }
        if (isset($_SESSION['loggedin'])) {

          if ($_SESSION['gebruiker_rol_id'] == 2) {
          ?>

            <li class="nav-item">
              <a class="nav-link" href="index.php?page=bekijkwedstrijden">Wedstrijden bekijken</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="index.php?page=wedstrijdentoevoegen">Wedstrijden toevoegen</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="index.php?page=teamstoevoegen">Teams toevoegen</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="index.php?page=teamsbekijken">Teams bekijken</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="index.php?page=toernooiaanmaken">Toernooi aanmaken</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="index.php?page=toernooienbekijken">Toernooi bekijken</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="php/logout.php">Logout</a>
            </li>

          <?php
          }
        }
        if (isset($_SESSION['loggedin'])) {

          if ($_SESSION['gebruiker_rol_id'] == 3) {
          ?>
            <li class="nav-item">
              <a class="nav-link" href="index.php?page=mijnwedstrijden">Mijn wedstrijden</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="php/logout.php">Logout</a>
            </li>
          <?php
          }
        }
        if (isset($_SESSION['loggedin'])) {

          if ($_SESSION['gebruiker_rol_id'] == 4) {
          ?>
            <li class="nav-item">
              <a class="nav-link" href="index.php?page=bekijkwedstrijden">Wedstrijden bekijken</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="index.php?page=actorenbekijken">Actoren bekijken</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="php/logout.php">Logout</a>
            </li>
          <?php
          }
        } else {
          ?>
          <li class="nav-item">
            <a class="nav-link" href="index.php?page=bekijkwedstrijden">Wedstrijden bekijken</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="index.php?page=login">Inloggen</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="index.php?page=registreren">Registreren</a>
          </li>
        <?php
        }

        ?>


      </ul>
    </div>
  </div>
</nav>