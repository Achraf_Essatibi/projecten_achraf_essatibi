<?php
$id = $_GET['id'];
$genres = $conn->query('SELECT * FROM genre');
$sql = "SELECT * FROM movies WHERE movie_id = ?";
$statement = $conn->prepare($sql);
$statement->execute([$id]);
$movie = $statement->fetch(PDO::FETCH_ASSOC);
?>

<div class="container mt-5">
    <h1>Edit movie</h1>
    <hr>
    <form action="php/editmovie.php" method="POST">
        <input type="hidden" name="movie_id" value="<?= $movie['movie_id']; ?>">
        <div class="form-group">
            <label for="movie_name">Movie</label>
            <input type="text" name="movie_name" id="movie_name" class="form-control" value="<?= $movie['movie_name']; ?>">
        </div>
        </br>
        <div class="form-floating mb-3">
            <select class="form-select" aria-label="Select" name="movie_genre_id">
                <?php foreach ($genres as $genre) { ?>
                    <option value="<?php echo $genre['genre_id']; ?>"><?php echo $genre['genre_name'];
                                                                    } ?></option>
            </select>
            <label for="Genre">Genre</label>
        </div>
        <div class="form-group">
            <label for="movie_duration">Duration</label>
            <input type="text" name="movie_duration" id="movie_duration" class="form-control" value="<?= $movie['movie_duration']; ?>">
        </div>
        </br>
        <div class="form-group">
            <label for="movie_description">Description</label>
            <input type="text" name="movie_description" id="movie_description" class="form-control" value="<?= $movie['movie_description']; ?>">
        </div>
        </br>

        <button type="submit" class="btn btn-primary mt-3">Save</button>
    </form>
</div>