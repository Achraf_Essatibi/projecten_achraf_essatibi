<?php require 'dbh.php';
session_start();

$user_id = $_SESSION['user_id'];
$performance_id = $_POST['performance_id'];


if (empty($performance_id)) {
    $_SESSION['message'] = "Reservation failed";
    header("Location: ../index.php?page=reserve");
    exit();
}

$sql = "INSERT INTO reservations (user_id, performance_id) VALUES (?,?)";
$conn->prepare($sql)->execute([$user_id, $performance_id]);

$_SESSION['messageSuccess'] = "You reserved succesfully";

header("Location:../index.php?page=reserve");
