<?php
require "connector.php";
session_start();
$toernooi_naam = $_POST['toernooi_naam'];
$toernooi_startdatum = $_POST['toernooi_startdatum'];

$sql = "SELECT * from toernooien WHERE toernooi_naam = ?";
$stmt = $conn->prepare($sql);
$stmt->execute([$toernooi_naam]);
$result = $stmt->fetch();

if ($result) {
    $_SESSION['message'] = "Het toernooi dat u wilt toevoegen bestaat al";
    header("Location: ../index.php?page=toernooiaanmaken");
    exit();
}

$sql = "INSERT INTO toernooien (toernooi_naam, toernooi_startdatum) VALUES 
(:toernooi_naam, :toernooi_startdatum)";
$data = [
    'toernooi_naam' => $toernooi_naam,
    'toernooi_startdatum' => $toernooi_startdatum
];
$conn->prepare($sql)->execute($data);
$_SESSION['messageSuccess'] = "Het team is toegevoegd";
header("Location:../index.php?page=toernooienbekijken");
