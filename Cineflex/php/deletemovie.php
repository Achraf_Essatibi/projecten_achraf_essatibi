<?php
require 'dbh.php';

$movie_id = $_GET['id'];

$sql = 'DELETE FROM movies WHERE movie_id= :movie_id';
$stmt = $conn->prepare($sql);
if ($stmt->execute([':movie_id' => $movie_id])) {

    header("Location:../index.php?page=moviemanagement");
}
