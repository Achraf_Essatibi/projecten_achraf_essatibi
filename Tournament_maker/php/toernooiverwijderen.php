<?php
require 'connector.php';

$toernooi_id = $_GET['id'];

$sql = 'DELETE FROM toernooien WHERE toernooi_id = :toernooi_id';
$stmt = $conn->prepare($sql);
if ($stmt->execute([':toernooi_id' => $toernooi_id])) {

    header("Location:../index.php?page=teamstoevoegen");
}
