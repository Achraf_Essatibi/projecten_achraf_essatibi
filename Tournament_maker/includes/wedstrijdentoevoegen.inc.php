<div class="container mt-5">
    <div class="col-md-4 offset-md-4">
        <form action="php/wedstrijdentoevoegen.php" method="POST">
            <h1>Wedstrijden toevoegen</h1>
            <label for="exampleFormControlTextarea1">Team 1</label>
            <div class="form-group">
                <select class="form-select" name="team1_id">
                    <?php
                    $sql = "SELECT * FROM teams";
                    $stmt = $conn->query($sql);
                    $stmt->execute();
                    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

                    foreach ($result as $team) { ?>
                        <option value="<?php echo $team['team_id']; ?>"><?php echo $team['team_naam']; ?></option>
                    <?php } ?>
                </select>
            </div>
            <label for="exampleFormControlTextarea1">Team 2</label>
            <div class="form-group">
                <select class="form-select" name="team2_id">
                    <?php
                    $sql = "SELECT * FROM teams";
                    $stmt = $conn->query($sql);
                    $stmt->execute();
                    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

                    foreach ($result as $team) { ?>
                        <option value="<?php echo $team['team_id']; ?>"><?php echo $team['team_naam']; ?></option>
                    <?php } ?>
                </select>
            </div>
            <label for="exampleFormControlTextarea1">Ronde</label>
            <div class="form-group">
                <select class="form-select" name="ronde_id">
                    <?php
                    $sql = "SELECT * FROM rondes";
                    $stmt = $conn->query($sql);
                    $stmt->execute();
                    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

                    foreach ($result as $rondes) { ?>
                        <option value="<?php echo $rondes['ronde_id']; ?>"><?php echo $rondes['ronde_naam']; ?></option>
                    <?php } ?>
                </select>
            </div>
            <label for="exampleFormControlTextarea1">Toernooi</label>
            <div class="form-group">
                <select class="form-select" name="toernooi_id">
                    <?php
                    $sql = "SELECT * FROM toernooien";
                    $stmt = $conn->query($sql);
                    $stmt->execute();
                    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

                    foreach ($result as $rondes) { ?>
                        <option value="<?php echo $rondes['toernooi_id']; ?>"><?php echo $rondes['toernooi_naam']; ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Datum</label>
                <input type="date" class="form-control" id="exampleInputPassword1" placeholder="Datum" name="wedstrijd_datum" required>
            </div>
            <label for="exampleFormControlTextarea1">Scheidsrechter</label>
            <div class="form-group">
                <select class="form-select" name="scheidsrechter_id">
                    <?php
                    $sql = "SELECT * FROM gebruikers WHERE gebruiker_rol_id = 3 AND gebruiker_is_active = 1 ";
                    $stmt = $conn->query($sql);
                    $stmt->execute();
                    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

                    foreach ($result as $scheids) { ?>
                        <option value="<?php echo $scheids['gebruiker_id']; ?>"><?php echo $scheids['gebruiker_voornaam'];
                                                                                echo " ";
                                                                                echo $scheids['gebruiker_achternaam']; ?></option>
                    <?php } ?>
                </select>
            </div>
            <button type="submit" class="btn btn-primary" style="margin-top: 10px;">Voeg wedstrijd toe</button>
        </form>
    </div>
</div>