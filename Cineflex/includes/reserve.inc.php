</br>

<?php
if (!isset($_SESSION['loggedin'])) {
    $_SESSION['message'] = 'Make sure to login.';
    header('Location: index.php?page=login');
    exit();
}


$sql = "SELECT * FROM performances 
        INNER JOIN movies ON performances.performance_movie_id = movies.movie_id
        INNER JOIN halls ON performances.performance_hall_id = halls.hall_id
        INNER JOIN locations ON halls.location_id = locations.location_id";
$stmt = $conn->prepare($sql);
$stmt->execute();
$performances = $stmt->fetchAll(PDO::FETCH_ASSOC);

// echo '<pre>';
//     print_r($voorstellingen);
// echo '</pre>';
?>

<div class="container mt-5">
    <div class="col-md-4 offset-md-4">

        <h1>Reserve</h1>
        <hr>

        <form action="php/reserve.php" method="POST">
            <div class="form-floating mb-3">
                <select onchange="check(this)" id="select" class="form-select" aria-label="Select" name="performance_id">
                    <option value="">Choose a performance</option>
                    <?php foreach ($performances as $performance) {

                        $sql = "SELECT * FROM reservations WHERE user_id = ? AND performance_id = ?";
                        $stmt = $conn->prepare($sql);
                        $stmt->execute([$_SESSION['user_id'], $performance['performance_id']]);
                        $result = $stmt->fetch(PDO::FETCH_ASSOC);

                        $disabled = "";
                        $addText = "";
                        if ($result) {
                            $disabled = "disabled";
                            $addText = "Already reserved";
                        }
                    ?>

                        <option <?= $disabled ?> value="<?php echo $performance['performance_id']; ?>"><?php echo $performance['movie_name']; ?> - <?php echo $performance['location_name']; ?> - <?= $addText ?></option>

                    <?php
                    }
                    ?>
                </select>
                <label for="floatingPassword">Performance</label>
            </div>
            <button disabled id="submit" class="btn btn-primary" type="submit">Reserve</button>
        </form>
        </br>
        </form>

    </div>
</div>

<script>
    function check(el) {
        if (el.value) {
            document.getElementById("submit").disabled = false;
        } else {
            document.getElementById("submit").disabled = true;
        }
    }
</script>