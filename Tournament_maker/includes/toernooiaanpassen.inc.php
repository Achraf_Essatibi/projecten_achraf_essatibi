<?php require "php/connector.php";
$id = $_GET['id'];
$sql = "SELECT * FROM toernooien WHERE toernooi_id = $id";
$stmt = $conn->prepare($sql);
$stmt->execute([$id]);

foreach ($conn->query($sql) as $value) {
    $value['toernooi_id'];
    $value['toernooi_naam'];
    $value['toernooi_startdatum'];
?>

    <div class="container mt-5">
        <div class="col-md-4 offset-md-4">
            <form action="php/toernooiaanpassen.php" method="POST">
                <input type="hidden" value="<?php echo $value['toernooi_id']; ?>" name="toernooi_id">
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Toernooi naam</label>
                    <input type="text" name="toernooi_naam" class="form-control" value="<?php echo $value['toernooi_naam']; ?>" onkeypress="return /[a-z]/i.test(event.key)">
                </div>
                <div class="form-group">
                    <label for="exampleInputMerk1">Toernooi startdatum</label>
                    <input type="date" name="toernooi_startdatum" class="form-control" value="<?php echo $value['toernooi_startdatum']; ?>" onkeypress="return /[a-z]/i.test(event.key)">
                </div>
                <button type="submit" style="margin-top:10px; margin-left: 30%;" class="btn btn-primary">Pas toernooi aan</button>
            </form>
        </div>
    </div>
<?php } ?>