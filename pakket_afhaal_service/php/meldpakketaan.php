<?php
session_start();
include 'connector.php';
if (isset($_POST['pakket_aanmelden'])) {
    $data = [
        'user_id' => $_POST['user_id'],
        'package_name' => $_POST['pakket_naam'],
        'package_important' => $_POST['pakket_spoed'],
        'package_size' => $_POST['pakket_afmetingen'],
        'package_price' => $_POST['pakket_totaalprijs'],
        'status_id' => 2,
    ];
    $sql = "INSERT INTO packages (user_id, package_name, package_important, package_size, package_price, status_id) VALUES 
    (:user_id, :package_name, :package_important, :package_size, :package_price, :status_id) ";
    $stmt = $conn->prepare($sql);
    $stmt->execute($data);

    header("location: ../index.php?page=bekijkpakket");
}
