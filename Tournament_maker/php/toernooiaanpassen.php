<?php require "connector.php";
session_start();
$toernooi_id = $_POST['toernooi_id'];
$toernooi_naam = $_POST['toernooi_naam'];
$toernooi_starttdatum = $_POST['toernooi_startdatum'];

$sql = "UPDATE toernooien SET toernooi_naam = :toernooi_naam, toernooi_startdatum = :toernooi_startdatum WHERE toernooi_id = $toernooi_id";
$data = [
    'toernooi_naam' => $toernooi_naam,
    'toernooi_startdatum' => $toernooi_starttdatum

];
$conn->prepare($sql)->execute($data);
$_SESSION['messageSuccess'] = "Het toernooi is aangepast";
header("Location:../index.php?page=toernooienbekijken ");
