<div class="container mt-5">
    <div class="col-md-4 offset-md-4">
        <form action="php/toernooiaanmaken.php" method="POST">
            <div class="form-group">
                <label for="exampleInputEmail1">Toernooi naam</label>
                <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Vul de naam van het team in" name="toernooi_naam" onkeypress="return /[a-z]/i.test(event.key)" required>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Start datum</label>
                <input type="date" class="form-control" id="exampleInputPassword1" placeholder="Vul de afkorting van het team in" name="toernooi_startdatum" required>
            </div>
            <button type="submit" style="margin-top:10px; margin-left: 33%;" class="btn btn-primary">Maak toernooi aan</button>
        </form>
    </div>
</div>