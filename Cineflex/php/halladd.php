<?php require 'dbh.php';
session_start();

$hall_number = $_POST['hall_number'];
$hall_row = $_POST['hall_row'];
$hall_chair = $_POST['hall_chair'];
$location_id = $_POST['location_id'];



if (empty($zaal_rij) || empty($zaal_stoel) || empty($locatie_id)) {
    $_SESSION['message'] = "Fill in all fields";
    // header("Location: ../index.php?page=zaaltoevoegen");
    exit();
}

$sql = "INSERT INTO halls (hall_number, hall_row, hall_chair, location_id) VALUES (?,?,?,?)";
$conn->prepare($sql)->execute([$hall_number, $hall_row, $$hall_chair, $location_id]);

$_SESSION['messageSuccess'] = "Hall is added succesfully";

header("Location:../index.php?page=zaaltoevoegen");
