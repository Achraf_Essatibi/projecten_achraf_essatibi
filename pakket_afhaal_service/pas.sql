-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Gegenereerd op: 29 mrt 2023 om 17:57
-- Serverversie: 10.4.24-MariaDB
-- PHP-versie: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pas`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `afmetingen`
--

CREATE TABLE `afmetingen` (
  `afmetingen_id` int(11) NOT NULL,
  `afmetingen` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `afmetingen`
--

INSERT INTO `afmetingen` (`afmetingen_id`, `afmetingen`) VALUES
(1, '0-10kg - 38 x 26.5 x 3.2 cm'),
(2, '10-30kg - 176 x 78 x 58 cm');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `packages`
--

CREATE TABLE `packages` (
  `package_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `package_name` varchar(50) NOT NULL,
  `package_important` varchar(50) NOT NULL,
  `package_size` varchar(250) NOT NULL,
  `package_price` varchar(50) NOT NULL,
  `status_id` int(11) NOT NULL,
  `claim_id` int(11) NOT NULL,
  `packet_problem` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `packages`
--

INSERT INTO `packages` (`package_id`, `user_id`, `package_name`, `package_important`, `package_size`, `package_price`, `status_id`, `claim_id`, `packet_problem`) VALUES
(1, 1, '', '1', '2', '62.92', 1, 0, NULL),
(2, 1, '', '1', '1', '33.976800000000004', 1, 0, NULL),
(3, 1, '', '1', '1', '25.482600000000005', 1, 0, NULL),
(4, 1, 'za3kof', '1', '2', '249.16320000000002', 5, 3, NULL),
(5, 1, 'yooo', '1', '1', '25.482600000000005', 2, 0, NULL),
(6, 2, 'puma', '1', '2', '199.33056000000002', 3, 3, NULL);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `statuses`
--

CREATE TABLE `statuses` (
  `status_id` int(11) NOT NULL,
  `status_pakket` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `statuses`
--

INSERT INTO `statuses` (`status_id`, `status_pakket`) VALUES
(2, 'Aangemeld '),
(3, 'Opdracht aangenomen '),
(4, 'Onderweg om op te halen '),
(5, 'Tijdelijke opslag '),
(6, 'Onderweg naar afleveradres '),
(7, 'Afgeleverd ');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(250) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `users`
--

INSERT INTO `users` (`user_id`, `username`, `email`, `password`, `role_id`) VALUES
(2, 'Gebruiker', 'gebruiker123@live.nl', '$2y$10$ttLi0MXTXaueKNOs/4Cc.eQZ7cFfXoqGnyZIukldiji94Y/P46op2', 3),
(3, 'Admin', 'admin123@live.nl', '$2y$10$0B2iuZhactMH.wK4GFJ7vuGVu4n3SAxAPx.1JCaDxDj/Jajznbsia', 1);

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `afmetingen`
--
ALTER TABLE `afmetingen`
  ADD PRIMARY KEY (`afmetingen_id`);

--
-- Indexen voor tabel `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`package_id`);

--
-- Indexen voor tabel `statuses`
--
ALTER TABLE `statuses`
  ADD PRIMARY KEY (`status_id`);

--
-- Indexen voor tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `afmetingen`
--
ALTER TABLE `afmetingen`
  MODIFY `afmetingen_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT voor een tabel `packages`
--
ALTER TABLE `packages`
  MODIFY `package_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT voor een tabel `statuses`
--
ALTER TABLE `statuses`
  MODIFY `status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT voor een tabel `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
