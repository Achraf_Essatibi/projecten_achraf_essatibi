</br>
<?php
if (isset($_SESSION['loggedin'])) {
  $_SESSION['message'] = 'Make sure to log out.';
  header('Location: index.php?page=home');
  exit();
}
?>

<div class="container mt-5">
  <div class="col-md-4 offset-md-4">

    <h1>Log in</h1>
    <hr>

    <form action="php/login.php" method="POST">
      <div class="form-floating mb-3" style="margin-top: 30px;">
        <input type="email" class="form-control" id="floatingInput" name="user_email" placeholder="Naam@example.com">
        <label for="floatingInput">Email</label>
      </div>
      <div class="form-floating mb-3">
        <input type="password" class="form-control" id="floatingPassword" name="user_password" placeholder="Wachtwoord">
        <label for="floatingPassword">Password</label>
      </div>
      <button class="btn btn-primary" type="submit">Log in</button>
    </form>
  </div>
</div>