<?php require 'php/connector.php';?>
<?php
session_start();
    if(isset($_GET['page']))
    {
        $page = $_GET['page'];
    }
    else
    {
        $page = 'home';
    }
?>
<head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<title>Pakket Afhaal Service</title>
<body>
<?php 
include 'includes/navbar.inc.php';
// if (ISSET($_SESSION['melding']))
// {
//     echo $_SESSION['melding'];
//     unset($_SESSION['melding']);
    
// }
    if (ISSET($_SESSION['message'])) {
      echo '<div class="container mt-3"><div class="alert alert-danger" role="alert">
                '.$_SESSION['message'].'
            </div></div>';

      unset($_SESSION['message']);
    }

    if (ISSET($_SESSION['messageSuccess'])) {
      echo '<div class="container mt-3"><div class="alert alert-success" role="alert">
                '.$_SESSION['messageSuccess'].'
            </div></div>';

      unset($_SESSION['messageSuccess']);
    }

include 'includes/'.$page.'.inc.php'; 
?>
</body>