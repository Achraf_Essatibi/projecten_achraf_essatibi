<?php
$sql = "SELECT * FROM reservations
INNER JOIN performances ON reservations.performance_id = performances.performance_id
INNER JOIN movies ON performances.performance_movie_id = movies.movie_id
INNER JOIN halls ON performances.performance_hall_id = halls.hall_id
INNER JOIN locations ON halls.location_id = locations.location_id
WHERE reservations.user_id = ?";
$stmt = $conn->prepare($sql);
$stmt->execute([$_SESSION['user_id']]);
$reservations  = $stmt->fetchAll(PDO::FETCH_ASSOC);

// echo '<pre>';
//     print_r($reserveringen);
// echo '</pre>';
?>

<div class="container mt-3">
    <h2>Reservations</h2>
    <hr>

    <div class="row">

        <?php
        foreach ($reservations as $key => $value) {
        ?>

            <div class="card mt-4">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <h5 class="card-title"><?= $value['movie_name']; ?></h5>
                            <p class="card-text"><?= $value['location_name']; ?></p>
                            <!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
                        </div>
                        <!-- <div class="col-md-3">
                        <div class="float-end">
                            <p><strong class="card-text">Duration: <?= $value['movie_duration']; ?></strong><p>
                            <p><strong class="card-text">Genre: <?= $value['genre_name']; ?></strong><p>
                        </div>
                    </div> 
                    <div class="col-md-3">
                        <div class="float-end">
                            <p><strong class="card-text">Location: <?= $value['location_name']; ?></strong><p>
                            <p><strong class="card-text">Hall: <?= $value['hall_id']; ?></strong><p>
                        </div>
                    </div>  -->
                    </div>
                </div>
            </div>


        <?php
        }
        ?>

    </div>

</div>