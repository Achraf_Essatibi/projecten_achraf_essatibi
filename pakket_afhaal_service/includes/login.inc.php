<?php 
  if(isset($_SESSION['melding']))
  {
    echo '<label class="text-danger">'.$_SESSION['melding'].'</label>';
    unset($_SESSION['melding']);
  }
?>
<form method="post" action="php/login.php">
    <div class="container mt-5" >
        <div class="row">
            <div class="col-md-4 offset-md-4">

                <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" name="login_email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Vul je emailadres in">
                    <small id="emailHelp" class="form-text text-muted">We zullen jouw informatie nooit met iemand delen.</small>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Wachtwoord</label>
                    <input type="password" name="login_password" class="form-control" id="exampleInputPassword1" placeholder="Vul je wachtwoord in">
                </div>
                <button type="submit" name="login_button" class="btn btn-primary">Login</button>
            </div>
        </div>
    </div>
</form>