<?php include 'connector.php'; 
$username = $_POST['username'];
$email = $_POST['email'];
$password = $_POST['password'];
$roleid = 3;        
  // Password hash
    $password_hash = password_hash($password, PASSWORD_BCRYPT);

if (isset($_POST['register'])) {    
$query = "INSERT INTO users (username, email, password, role_id) VALUES (?, ?, ?, ?)";
$stmt = $conn->prepare($query);
$stmt->execute([$username, $email, $password_hash, $roleid] );
header("location: ../index.php?page=home");

}
?>
