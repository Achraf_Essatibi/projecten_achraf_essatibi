<?php
require 'php/dbh.php';

if ($_SESSION['user_role'] != 4) {
  $_SESSION['message'] = 'You dont have enough rights to visit this page';
  header('Location: index.php?page=home');
  exit();
}

$sql = 'SELECT * FROM movies';
$statement = $conn->prepare($sql);
$statement->execute();
$people = $statement->fetchAll(PDO::FETCH_ASSOC);
?>

<div class="container">
  <div class="card mt-5">
    <div class="card-header">
      <h2>Movie management</h2>
    </div>

    <div class="card-body">
      <table class="table table-bordered">
        <tr>
          <th>Movie</th>
          <th>Genre</th>
          <th>Duration</th>
          <th>Description</th>
          <th>Actions</th>
        </tr>
        <?php foreach ($people as $person) : ?>
          <tr>
            <td><?= $person['movie_name']; ?></td>
            <td><?= $person['movie_genre_id']; ?></td>
            <td><?= $person['movie_duration']; ?></td>
            <td><?= $person['movie_description']; ?></td>
            <td>
              <a href="index.php?page=movie_edit&id=<?= $person['movie_id']; ?>" class="btn btn-warning">Edit</a>

              <a onclick="return confirm('Are you sure that you want to delete this movie?')" href="php/deletemovie.php?id=<?= $person['movie_id'] ?>" class='btn btn-danger'>Delete</a>
            </td>
          </tr>
        <?php endforeach; ?>
      </table>
    </div>
  </div>
</div>