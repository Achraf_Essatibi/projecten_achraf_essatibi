<?php
require 'php/connector.php';

$sql = 'SELECT * FROM toernooien';
$statement = $conn->prepare($sql);
$statement->execute();
$people = $statement->fetchAll(PDO::FETCH_ASSOC);
?>

<div class="container">
    <div class="card mt-5">
        <div class="card-header">
            <h2>Toernooi management</h2>
        </div>

        <div class="card-body">
            <table class="table table-bordered">
                <tr>
                    <th>Toernooi naam</th>
                    <th>Toernooi startdatum</th>
                </tr>
                <?php foreach ($people as $person) : ?>
                    <tr>
                        <td><?= $person['toernooi_naam']; ?></td>
                        <td><?= $person['toernooi_startdatum']; ?></td>
                        <td>
                            <a href="index.php?page=toernooiaanpassen&id=<?= $person['toernooi_id']; ?>" class="btn btn-warning">Edit</a>

                            <a onclick="return confirm('Weet je zeker dat je dit team wilt verwijderen')" href="php/toernooiverwijderen.php?id=<?= $person['toernooi_id'] ?>" class='btn btn-danger'>Verwijderen</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </div>
</div>