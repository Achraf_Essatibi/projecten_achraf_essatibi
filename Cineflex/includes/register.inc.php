</br>
<?php
if (isset($_SESSION['loggedin'])) {
  $_SESSION['message'] = 'Make sure to login.';
  header('Location: index.php?page=home');
  exit();
}
?>

<div class="container mt-5">
  <div class="col-md-4 offset-md-4">

    <h1>Register</h1>
    <hr>

    <form action="php/register.php" method="POST">
      <div class="form-floating mb-3" style="margin-top: 30px;">
        <input type="email" class="form-control" id="floatingInput" name="user_email" placeholder="Naam@example.com">
        <label for="floatingInput">Email</label>
      </div>
      <div class="form-floating mb-3">
        <input type="password" class="form-control" id="floatingPassword" name="user_password" placeholder="Wachtwoord">
        <label for="floatingPassword">Password</label>
      </div>
      <div class="form-floating mb-3">
        <input type="text" class="form-control" id="floatingPassword" name="user_firstname" placeholder="Voornaam">
        <label for="floatingPassword">First name</label>
      </div>
      <div class="form-floating mb-3">
        <input type="text" class="form-control" id="floatingPassword" name="user_lastname" placeholder="Achternaam">
        <label for="floatingPassword">Last name</label>
      </div>
      <button class="btn btn-primary" type="submit">Register</button>
    </form>
  </div>
</div>