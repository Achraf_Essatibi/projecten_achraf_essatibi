<?php
require 'connector.php';

$team_id = $_GET['id'];

$sql = 'DELETE FROM teams WHERE team_id = :team_id';
$stmt = $conn->prepare($sql);
if ($stmt->execute([':team_id' => $team_id])) {

    header("Location:../index.php?page=teamstoevoegen");
}
