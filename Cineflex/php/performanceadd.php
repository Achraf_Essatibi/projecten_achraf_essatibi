<?php require 'dbh.php';
session_start();

$hall_id = $_POST['hall_id'];
$movie_id = $_POST['movie_id'];
$director_id = $_POST['director_id'];


if (empty($hall_id) || empty($movie_id) || empty($director_id)) {
    $_SESSION['message'] = "Fill in all fields";
    header("Location: ../index.php?page=addperformance");
    exit();
}

$sql = "INSERT INTO performances (performance_hall_id, performance_movie_id, performance_director_id) VALUES (?,?,?)";
$conn->prepare($sql)->execute([$hall_id, $movie_id, $director_id]);

$_SESSION['messageSuccess'] = "Performance is added succesfully";

header("Location:../index.php?page=addperformance");
