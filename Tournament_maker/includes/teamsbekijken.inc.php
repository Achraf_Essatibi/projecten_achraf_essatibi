<?php
require 'php/connector.php';

$sql = 'SELECT * FROM teams';
$statement = $conn->prepare($sql);
$statement->execute();
$people = $statement->fetchAll(PDO::FETCH_ASSOC);
?>

<div class="container">
    <div class="card mt-5">
        <div class="card-header">
            <h2>Team management</h2>
        </div>

        <div class="card-body">
            <table class="table table-bordered">
                <tr>
                    <th>Team naam</th>
                    <th>Afkorting</th>
                    <th>Team foto</th>
                    <th>Acties</th>
                </tr>
                <?php foreach ($people as $person) : ?>
                    <tr>
                        <td><?= $person['team_naam']; ?></td>
                        <td><?= $person['team_afkorting']; ?></td>
                        <?php echo '<td><img src= "images/' . $person['team_foto'] . '" style="height: 150px;"</td>'; ?>
                        <td>
                            <a href="index.php?page=teamsaanpassen&id=<?= $person['team_id']; ?>" class="btn btn-warning">Edit</a>

                            <a onclick="return confirm('Weet je zeker dat je dit team wilt verwijderen')" href="php/teamverwijderen.php?id=<?= $person['team_id'] ?>" class='btn btn-danger'>Verwijderen</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </div>
</div>