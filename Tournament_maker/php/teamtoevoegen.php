<?php
require "connector.php";
session_start();
$team_naam = $_POST['team_naam'];
$team_afkorting = $_POST['team_afkorting'];
$team_foto = $_POST['team_foto'];

$sql = "SELECT * from teams WHERE team_naam = ?";
$stmt = $conn->prepare($sql);
$stmt->execute([$team_naam]);
$result = $stmt->fetch();

if ($result) {
    $_SESSION['message'] = "Het team dat u wilt toevoegen bestaat al";
    header("Location: ../index.php?page=teamstoevoegen");
    exit();
}

$sql = "INSERT INTO teams (team_naam, team_afkorting, team_foto) VALUES 
(:team_naam, :team_afkorting, :team_foto)";
$data = [
    'team_naam' => $team_naam,
    'team_afkorting' => $team_afkorting,
    'team_foto' => $team_foto
];
$conn->prepare($sql)->execute($data);
$_SESSION['messageSuccess'] = "Het team is toegevoegd";
header("Location:../index.php?page=teamsbekijken");
