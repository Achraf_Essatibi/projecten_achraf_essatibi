</br>
<?php
if (!isset($_SESSION['loggedin'])) {
  $_SESSION['message'] = 'Make sure to login.';
  header('Location: index.php?page=login');
  exit();
}

$stmt = $conn->prepare("SELECT * FROM halls INNER JOIN locations ON halls.location_id = locations.location_id");
$stmt->execute();
$halls = $stmt->fetchAll();

$stmt = $conn->prepare("SELECT * FROM movies");
$stmt->execute();
$movies = $stmt->fetchAll();

$stmt = $conn->prepare("SELECT * FROM directors");
$stmt->execute();
$directors = $stmt->fetchAll();

// echo '<pre>';
//     print_r($zalen);
// echo '</pre>';
?>

<div class="container mt-5">
  <div class="col-md-4 offset-md-4">

    <h1>Add performance</h1>
    <hr>

    <form action="php/performanceadd.php" method="POST">
      <div class="form-floating mb-3">
        <select class="form-select" aria-label="Select" name="movie_id">
          <?php foreach ($movies as $movie) { ?>
            <option value="<?php echo $movie['movie_id']; ?>"><?php echo $movie['movie_name'];
                                                            } ?></option>
        </select>
        <label for="floatingPassword">Movie</label>
      </div>
      <div class="form-floating mb-3">
        <select class="form-select" aria-label="Select" name="hall_id">
          <?php foreach ($halls as $hall) { ?>
            <option value="<?php echo $hall['hall_id']; ?>"><?php echo $hall['hall_number']; ?> - <?php echo $hall['location_name'];
                                                                                                } ?></option>
        </select>
        <label for="floatingPassword">Hall</label>
      </div>
      <div class="form-floating mb-3">
        <select class="form-select" aria-label="Select" name="director_id">
          <?php foreach ($directors as $director) { ?>
            <option value="<?php echo $director['director_id']; ?>"><?php echo $director['director_name'];
                                                                  } ?></option>
        </select>
        <label for="floatingPassword">Director</label>
      </div>
      <button class="btn btn-primary" type="submit">Add</button>
    </form>
    </br>
    </form>

  </div>
</div>