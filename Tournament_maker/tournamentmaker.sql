-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Gegenereerd op: 29 mrt 2023 om 17:59
-- Serverversie: 10.4.24-MariaDB
-- PHP-versie: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tournamentmaker`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `gebruikers`
--

CREATE TABLE `gebruikers` (
  `gebruiker_id` int(11) NOT NULL,
  `gebruiker_voornaam` varchar(50) NOT NULL,
  `gebruiker_achternaam` varchar(50) NOT NULL,
  `gebruiker_gebruikersnaam` varchar(50) NOT NULL,
  `gebruiker_email` varchar(255) NOT NULL,
  `gebruiker_wachtwoord` varchar(255) NOT NULL,
  `gebruiker_rol_id` int(11) NOT NULL,
  `gebruiker_is_active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `gebruikers`
--

INSERT INTO `gebruikers` (`gebruiker_id`, `gebruiker_voornaam`, `gebruiker_achternaam`, `gebruiker_gebruikersnaam`, `gebruiker_email`, `gebruiker_wachtwoord`, `gebruiker_rol_id`, `gebruiker_is_active`) VALUES
(1, 'Achraf', 'Essatibi', 'Achraf_026', 'a.essatibi@hotmail.com', '$2y$10$9XnW1z4HtQ7khAmI8PFaZ.X4ouDEnGRicBPa3URR989ZpttgpCpPm', 1, 1),
(4, 'Medewerker', 'jatoch', 'MedewerkerTM', 'Medewerker@gmail.com', '$2y$10$/rgP1l7EHvYH1um506CwT.eBEpG/oBvogNzBXw5k3pAHYudzGC7P6', 2, 1),
(5, 'Danny', 'Makkelie', 'DMS_tm', 'DMS_tm@live.nl', 'Scheids123', 3, 1),
(6, 'Bas', 'Nijhuis', 'BH_tm', 'BH_tm@gmail.com', '$2y$10$Ja/W1vo3fDIuWPxXQWZ6gOP1mRPnbvS1UTdDL650y817hvB3aWFYe', 3, 1),
(9, 'Beheerder', 'der', 'BeheerderTM', 'Beheerder@gmail.com', '$2y$10$o3ey2qiMOO8K2.QRXrzVK.leDUT/XuBR5td1urw2SRkry46Da8M/G', 4, 1),
(10, 'Achraf', 'ewefw', 'efwfewefw', 'a.essatibi2@hotmail.com', '$2y$10$z4ab6k621U.M.ypD7IP42evaTNxA3XHMLa0jgA9CIgWtit40ps/3m', 1, 1),
(11, 'Scheids', 'Rechter', 'scheidsrechter123', 'scheidsrechter123@live.nl', '$2y$10$l53.yBHUiLKuCI97Ufoxf.j/YoE.vOShnsaZeX4BCKyEykhqM1j72', 3, 1),
(12, 'Be', 'Heerder', 'beheerder123', 'beheerder123@live.nl', '$2y$10$VOfi7NVZXoDdDrYGzgww.uS.DCQgIds4vQWBmh4nGlvD4E9lwQKE6', 4, 1),
(13, 'mede', 'werker', 'medewerker123', 'medewerker123@live.nl', '$2y$10$KwLf8nmaJriK/NsIo6KajOFpN71M1KN/NAwQZtwr4wo2QJ5vB7Q4i', 2, 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `rollen`
--

CREATE TABLE `rollen` (
  `rol_id` int(11) NOT NULL,
  `rol_naam` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `rollen`
--

INSERT INTO `rollen` (`rol_id`, `rol_naam`) VALUES
(1, 'Gebruiker'),
(2, 'Medewerker'),
(3, 'Scheidsrechter'),
(4, 'Beheerder');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `rondes`
--

CREATE TABLE `rondes` (
  `ronde_id` int(11) NOT NULL,
  `ronde_naam` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `rondes`
--

INSERT INTO `rondes` (`ronde_id`, `ronde_naam`) VALUES
(1, 'Groepsfase'),
(2, 'Achtste finales'),
(3, 'Kwartfinales'),
(4, 'halve finales'),
(5, 'Finale ');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `teams`
--

CREATE TABLE `teams` (
  `team_id` int(11) NOT NULL,
  `team_naam` varchar(50) NOT NULL,
  `team_afkorting` varchar(50) NOT NULL,
  `team_foto` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `teams`
--

INSERT INTO `teams` (`team_id`, `team_naam`, `team_afkorting`, `team_foto`) VALUES
(1, 'Real Madrid', 'RMA', 'real_madrid.jpg'),
(2, 'Fc barcelona', 'BAR', 'fc_barcelona.png');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `teams_tournaments`
--

CREATE TABLE `teams_tournaments` (
  `team_id` int(11) NOT NULL,
  `toernooi_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `toernooien`
--

CREATE TABLE `toernooien` (
  `toernooi_id` int(11) NOT NULL,
  `toernooi_naam` varchar(50) NOT NULL,
  `toernooi_startdatum` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `toernooien`
--

INSERT INTO `toernooien` (`toernooi_id`, `toernooi_naam`, `toernooi_startdatum`) VALUES
(1, 'Champions league', '2022-11-30');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `wedstrijden`
--

CREATE TABLE `wedstrijden` (
  `wedstrijd_id` int(11) NOT NULL,
  `toernooi_id` int(11) NOT NULL,
  `team1_id` int(11) DEFAULT NULL,
  `team2_id` int(11) NOT NULL,
  `score_team1` int(11) DEFAULT NULL,
  `score_team2` int(11) DEFAULT NULL,
  `ronde_id` int(11) NOT NULL,
  `wedstrijd_datum` date NOT NULL,
  `scheidsrechter_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `wedstrijden`
--

INSERT INTO `wedstrijden` (`wedstrijd_id`, `toernooi_id`, `team1_id`, `team2_id`, `score_team1`, `score_team2`, `ronde_id`, `wedstrijd_datum`, `scheidsrechter_id`) VALUES
(2, 1, 1, 2, 0, 0, 2, '2022-11-22', 5),
(3, 1, 1, 2, NULL, NULL, 1, '2022-11-26', 5),
(5, 1, 1, 2, 7, 5, 3, '2022-11-24', 5);

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `gebruikers`
--
ALTER TABLE `gebruikers`
  ADD PRIMARY KEY (`gebruiker_id`),
  ADD KEY `role_id` (`gebruiker_rol_id`);

--
-- Indexen voor tabel `rollen`
--
ALTER TABLE `rollen`
  ADD PRIMARY KEY (`rol_id`);

--
-- Indexen voor tabel `rondes`
--
ALTER TABLE `rondes`
  ADD PRIMARY KEY (`ronde_id`);

--
-- Indexen voor tabel `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`team_id`);

--
-- Indexen voor tabel `teams_tournaments`
--
ALTER TABLE `teams_tournaments`
  ADD PRIMARY KEY (`team_id`),
  ADD KEY `tournament_id` (`toernooi_id`);

--
-- Indexen voor tabel `toernooien`
--
ALTER TABLE `toernooien`
  ADD PRIMARY KEY (`toernooi_id`);

--
-- Indexen voor tabel `wedstrijden`
--
ALTER TABLE `wedstrijden`
  ADD PRIMARY KEY (`wedstrijd_id`),
  ADD UNIQUE KEY `round_id` (`ronde_id`),
  ADD KEY `team1_id` (`team1_id`),
  ADD KEY `team1_id_2` (`team1_id`),
  ADD KEY `team2_id` (`team2_id`),
  ADD KEY `referee_id` (`scheidsrechter_id`),
  ADD KEY `toernooi_id` (`toernooi_id`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `gebruikers`
--
ALTER TABLE `gebruikers`
  MODIFY `gebruiker_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT voor een tabel `rollen`
--
ALTER TABLE `rollen`
  MODIFY `rol_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT voor een tabel `rondes`
--
ALTER TABLE `rondes`
  MODIFY `ronde_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT voor een tabel `teams`
--
ALTER TABLE `teams`
  MODIFY `team_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT voor een tabel `teams_tournaments`
--
ALTER TABLE `teams_tournaments`
  MODIFY `team_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT voor een tabel `toernooien`
--
ALTER TABLE `toernooien`
  MODIFY `toernooi_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT voor een tabel `wedstrijden`
--
ALTER TABLE `wedstrijden`
  MODIFY `wedstrijd_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Beperkingen voor geëxporteerde tabellen
--

--
-- Beperkingen voor tabel `gebruikers`
--
ALTER TABLE `gebruikers`
  ADD CONSTRAINT `gebruikers_ibfk_1` FOREIGN KEY (`gebruiker_rol_id`) REFERENCES `rollen` (`rol_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Beperkingen voor tabel `teams_tournaments`
--
ALTER TABLE `teams_tournaments`
  ADD CONSTRAINT `teams_tournaments_ibfk_1` FOREIGN KEY (`team_id`) REFERENCES `teams` (`team_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `teams_tournaments_ibfk_2` FOREIGN KEY (`toernooi_id`) REFERENCES `toernooien` (`toernooi_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Beperkingen voor tabel `wedstrijden`
--
ALTER TABLE `wedstrijden`
  ADD CONSTRAINT `wedstrijden_ibfk_1` FOREIGN KEY (`scheidsrechter_id`) REFERENCES `gebruikers` (`gebruiker_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `wedstrijden_ibfk_2` FOREIGN KEY (`team1_id`) REFERENCES `teams` (`team_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `wedstrijden_ibfk_3` FOREIGN KEY (`team2_id`) REFERENCES `teams` (`team_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `wedstrijden_ibfk_4` FOREIGN KEY (`ronde_id`) REFERENCES `rondes` (`ronde_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `wedstrijden_ibfk_5` FOREIGN KEY (`toernooi_id`) REFERENCES `toernooien` (`toernooi_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
