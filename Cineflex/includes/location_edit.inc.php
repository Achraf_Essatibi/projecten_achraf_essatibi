<?php
$id = $_GET['id'];
$locaties = $conn->query('SELECT * FROM locations');
$sql = "SELECT * FROM locations WHERE location_id = ?";
$statement = $conn->prepare($sql);
$statement->execute([$id]);
$location = $statement->fetch(PDO::FETCH_ASSOC);
?>

<div class="container mt-5">
    <h1>Edit movie</h1>
    <hr>
    <form action="php/location_edit.php" method="POST">
        <input type="hidden" name="location_id" value="<?= $location['location_id']; ?>">
        <div class="form-group">
            <label for="location_name">Location name</label>
            <input type="text" name="location_name" id="location_name" class="form-control" value="<?= $location['location_name']; ?>">
        </div>
        </br>
        <div class="form-group">
            <label for="location_street">Street</label>
            <input type="text" name="location_street" id="locatie_straat" class="form-control" value="<?= $location['location_street']; ?>">
        </div>
        </br>
        <div class="form-group">
            <label for="location_housenumber">House number</label>
            <input type="text" name="location_housenumber" id="location_housenumber" class="form-control" value="<?= $location['location_housenumber']; ?>">
        </div>
        </br>
        <div class="form-group">
            <label for="location_postalcode">Postal code</label>
            <input type="text" name="location_postalcode" id="locatie_postcode" class="form-control" value="<?= $location['location_postalcode']; ?>">
        </div>
        </br>
        <div class="form-group">
            <label for="locatie_place">place</label>
            <input type="text" name="location_place" id="locatie_plaats" class="form-control" value="<?= $location['location_place']; ?>">
        </div>

        <button type="submit" class="btn btn-primary mt-3">Save</button>
    </form>
</div>