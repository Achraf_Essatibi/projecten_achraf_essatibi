<?php
require 'dbh.php';

$location_id = $_GET['id'];

$sql = 'DELETE FROM locations WHERE location_id = :location_id';
$stmt = $conn->prepare($sql);
if ($stmt->execute([':location_id' => $location_id])) {

    header("Location:../index.php?page=locationmanagement");
}
