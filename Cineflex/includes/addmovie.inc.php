</br>
<?php
if (!isset($_SESSION['loggedin'])) {
  $_SESSION['message'] = 'Make sure to login.';
  header('Location: index.php?page=login');
  exit();
}
$stmt = $conn->prepare("SELECT * FROM genre");
$stmt->execute();
$genres = $stmt->fetchAll();
?>

<div class="container mt-5">
  <div class="col-md-4 offset-md-4">

    <h1>Add movie</h1>
    <hr>

    <form action="php/movieadd.php" method="POST">
      <div class="form-floating mb-3" style="margin-top: 30px;">
        <input type="text" class="form-control" id="floatingInput" name="movie_name" placeholder="Moviename">
        <label for="floatingInput">Movie name</label>
      </div>
      <div class="form-floating mb-3">
        <select class="form-select" aria-label="Select" name="movie_genre_id">
          <?php foreach ($genres as $genre) { ?>
            <option value="<?php echo $genre['genre_id']; ?>"><?php echo $genre['genre_name'];
                                                            } ?></option>
        </select>
        <label for="floatingPassword">Genre</label>
      </div>
      <div class="form-floating mb-3">
        <input type="text" class="form-control" id="floatingInput" name="movie_duration" placeholder="Duration">
        <label for="floatingPassword">Duration in hours</label>
      </div>
      <div class="form-floating mb-3">
        <input type="text" class="form-control" id="floatingInput" name="movie_description" placeholder="Description">
        <label for="floatingPassword">Description</label>
      </div>
      <button class="btn btn-primary" type="submit">Add</button>
    </form>
    </br>
    </form>

  </div>
</div>